import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DetailPage } from '../detail/detail';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  posts= [];
  private DetailPage;
  constructor(public navCtrl: NavController,private remoteServiceProvider: RemoteServiceProvider) {
    this.remoteServiceProvider.getPosts().subscribe((data)=>{
            this.posts = data;
        });
        this.DetailPage = DetailPage;
  }
  showButton(post){
    console.log(post);
    this.navCtrl.push(this.DetailPage,{post:post});
  }
}
